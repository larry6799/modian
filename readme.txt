1.先运行 Test.java ，得到全部的链接地址的 id 存入 id.txt,
    此过程去掉了在预约阶段的数据
  🇺每个链接为： "https://zhongchou.modian.com/item/"+ id +".html"
    示例：https://zhongchou.modian.com/item/3.html


2. id.txt 得到之后，再运行 EveryProject.java，遍历获取每个项目主页上面的信息，
   根据页面上的 众筹成功/众筹结束/看好创意/立即购买支持等信息获取项目的信息，
   获取到的信息重定向输出到 out.txt

3.  将 out.txt 中的数据复制到 excel 中

其他说明：
* 使用的语言为 java ，库为 jsoup，参考文档： https://www.open-open.com/jsoup/
* 由于没有ip代理，所以降低了抓取频率，时间设置长一些，访问时间采用随机数
* 抓取过程中可能会存在网络问题导致无法链接，会抛出异常，然后将没有成功的页面再次抓取
* Excel 中的 不明情况 ，我看了情况是 项目主动或者被动终止等情况，不算入成功/失败的众筹项目。